use academiax2;

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '2');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '3');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '4');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '5');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '6');


-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '7');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '8');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '9');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '10');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '11');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '12');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '13');

-- INSERT INTO `ordenadores` (`idordenadores`, `modelo`, `marca`, `anyo_compra`, `numero`) 
-- VALUES (NULL, '001-asd-009 BC', 'alien-ware', '2018-1-6', '14');

-- SELECT descripcion,nombre FROM ediciones e 
-- join cursos c on e.cursos_idcursos=c.idcursos
-- join matricula m on m.ediciones_idediciones=e.idediciones
-- join alumnos a on a.idalumnos=m.alumnos_idalumnos
-- where e.idediciones=7;


-- CONSULTAS  A REALIZAR:
-- ======================




-- Descripcion del curso, Modelo, marca de ordenador, numero de ordenador, nombre del alumno


-- SELECT o.modelo, o.marca, o.numero, a.nombre, c.descripcion
-- FROM ordenadores o 
-- JOIN matricula m ON (o.idordenadores = m.ordenadores_idordenadores)
-- JOIN alumnos a ON (m.alumnos_idalumnos = a.idalumnos)
-- JOIN ediciones e ON (m.ediciones_idediciones = e.idediciones)
-- JOIN cursos c ON (c.idcursos = e.cursos_idcursos)


-- Nombre de profesor y nombre de alumno

-- SELECT p.nombre as n_profesor, a.nombre as nombre_alumnos
-- FROM profesores as p
-- JOIN ediciones as e ON p.idprofesores = e.profesores_idprofesores
-- JOIN matricula as m ON e.idediciones = m.ediciones_idediciones
-- JOIN alumnos as a ON m.alumnos_idalumnos = a.idalumnos;

-- select p.nombre as n_profesor, a.nombre as nombre_alumno from ediciones as e
-- inner join profesores as p on e.profesores_idprofesores = p.idprofesores
-- inner join matricula as m on m.ediciones_idediciones = e.idediciones
-- inner join alumnos as a on m.alumnos_idalumnos = a.idalumnos;

-- Lenguaje, alumno, marca de ordenador

-- SELECT a.nombre as nombre_alumno, o.marca as marca_pc, c.Lenguaje as idioma
-- FROM matricula m
-- JOIN alumnos a ON m.alumnos_idalumnos = a.idalumnos
-- JOIN ordenadores o ON m.ordenadores_idordenadores = o.idordenadores
-- JOIN ediciones e ON m.ediciones_idediciones = e.idediciones
-- JOIN cursos c ON e.cursos_idcursos = c.idcursos;

-- Descripción del curso, fecha de inicio, número de alumnos matriculados

-- SELECT e.finicio, c.descripcion, COUNT(m.idmatricula) as numero
-- FROM ediciones e
-- JOIN cursos c ON e.cursos_idcursos = c.idcursos
-- JOIN matricula m ON e.idediciones = m.ediciones_idediciones
-- GROUP BY e.idediciones
-- order by numero desc;