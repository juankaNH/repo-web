use vgames;


-- Recuento total de registros de ventas (número de registros de la tabla) 

-- select count(*) as 'numero total' 
-- from fullcsv ;

-- Distintos tipos de plataformas (relación) 

-- select distinct platform 
-- from fullcsv;

-- Número de plataformas distintas (número de registros) 

-- select count(distinct platform) 
-- from fullcsv;

-- Suma de ventas totales para la plataforma “PSP” (cantidad en M$) 

-- select sum(Global_Sales) 
-- from fullcsv 
-- where platform="psp";

-- Suma de ventas totales en europa, norte américa y japón (cantidad en M$) 

-- select sum(NA_Sales+EU_Sales+JP_Sales) 
-- from fullcsv ;

-- Suponiendo la población actual, el "continente" con mayor índice de ventas por cápita 

-- select sum(NA_Sales)/325.7 as 'venta_por_capita_NA', sum(EU_Sales)/741.4 as 'venta_por_capita_EU', sum(JP_Sales)/126.8 as 'venta_por_capita_JP' 
-- from fullcsv ;

-- La suma de ventas en cada continente, solo para los géneros: racing, sports y simulation (los 3 sumados) 

-- select sum(NA_Sales) as 'venta_por_capita_NA', sum(EU_Sales) as 'venta_por_capita_EU', sum(JP_Sales) as 'venta_por_capita_JP' 
-- from fullcsv 
-- where genre in ("racing","sports","simulation") ;

-- select platform, count(*), sum(Global_Sales) 
-- from fullcsv 
-- where platform in ("ps2","ps3") 
-- group by platform;

-- select platform as `plataforma`, sum(Global_Sales) as 'ventas por año', Year_of_release as 'ano'
-- from fullcsv
-- where Year_of_release >= 2008 and Year_of_release <= 2010
-- group by plataforma, ano
-- order by ano, plataforma asc;

-- select sum(Global_Sales) as 'ventas_totales', Year_of_release as 'año_de_lanzamiento'
-- from fullcsv
-- where genre='sports'
-- group by Year_of_release
-- order by Year_of_release asc;

-- select sum(Global_Sales) as 'ventas_totales', Year_of_release as 'año_de_lanzamiento'
-- from fullcsv
-- where genre='Strategy'
-- group by Year_of_release
-- order by Year_of_release asc;

-- select name, platform, Year_of_release as 'año_De_lanzamiento'
-- from fullcsv
-- where platform='pc' and Year_of_release=0;

-- select sum(NA_Sales) as 'ameria', sum(EU_Sales) as 'europa', sum(JP_Sales) as 'japon', genre
-- from fullcsv
-- where genre='sports' or genre='racing' or genre='simulation'
-- group by genre
-- order by genre;

-- select name, sum(NA_Sales + EU_Sales + JP_Sales) as 'total', genre, Year_of_release
-- from fullcsv
-- group by name
-- order by total desc
-- limit 10;

-- select platform, sum(Global_Sales) total 
-- from fullcsv
-- group by platform
-- having total > 500
-- order by total desc;

-- select platform, count(*) titulos 
-- from fullcsv
-- group by platform
-- having titulos between 100 and 500
-- order by titulos desc;

-- select Year_of_release as 'ano',name, Global_Sales
-- from fullcsv
-- where name like '%pokemon%'
-- group by ano 
-- order by Global_Sales desc
-- limit 1;

-- select name, platform, sum(NA_Sales + EU_Sales + JP_Sales) as 'total', Year_of_release as 'ano'
-- from fullcsv
-- where platform in ("ps","ps2","ps3","ps4")
-- group by ano
-- order by ano;

